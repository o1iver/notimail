from flask import Flask, request
from mongokit import Connection, Document

import time
import calendar
import requests

# Configuration
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017

MAILGUN_KEY = 'key-1hpq77mqcqd$gv51q0'


# App
app = Flask(__name__)
app.config.from_object(__name__)

# DB connection
connection = Connection(app.config['MONGODB_HOST']
                       ,app.config['MONGODB_PORT'])

# Util
def current_epoch():
    return calendar.timegm(time.gmtime())

# Models
@connection.register
class NotiReq(Document):
    __collection__ = 'NotiReqs'
    __database__   = 'mainDB2'
    use_dot_notation = True
    structure = {
        'email': unicode,
        'subject': unicode,
        'body': unicode,
        'seconds': int,
        'sent': int,
        }

    validators = {
        }

    @property
    def due(self,times=None):
        current_time = current_epoch()
        return (current_time - self.sent) >= self.seconds

    def to_email(self):
        pass

    def __repr__(self):
        return '<NotiReq for %s>' % self.email

def create_noti_req(email,subject,body,seconds,sent=None):
    nr = connection.NotiReq()
    nr.email = email
    nr.subject = subject
    nr.body = body
    nr.seconds = seconds
    nr.sent = sent or current_epoch()
    nr.save()
    return nr

def all_not_reqs():
    return list(connection.NotiReq.find())

def due_noti_reqs():
    return filter(lambda nr: nr.due,list(connection.NotiReq.find()))

def send_email(from_,to,subject,body):
    if type(to) is not list:
        to = [to]

    return requests.post("https://api.mailgun.net/v2/o1iver.mailgun.org/messages"
                        ,auth=("api", app.config['MAILGUN_KEY'])
                        ,data={"from": from_
                              ,"to": to 
                              ,"subject": subject 
                              ,"text": body})

def send_notification(noti_req):
    return send_email(noti_req.email
                     ,noti_req.email
                     ,noti_req.subject
                     ,noti_req.body)



# Controllers

@app.route('/', methods=["GET"])
def home_get():
    return u'''
        <html>
            <form name="newNotificationRequest" action="/" method="POST">
                Every N seconds: <input type="text" name="seconds"><br />
                Email address: <input type="text" name="email"><br />
                Subject: <input type="text" name="subject"><br />
                Body:<br />
                <textarea rows="4" cols="40"  name="body">
                </textarea><br />
                <input type="submit" value="Submit">
            </form>
        </html>'''

@app.route('/', methods=["POST"])
def home_post():
    create_noti_req(unicode(request.form['email'])
                   ,unicode(request.form['subject'])
                   ,unicode(request.form['body'])
                   ,int(request.form['seconds']))
    return 'Requested posted!'


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=80,debug=True)

